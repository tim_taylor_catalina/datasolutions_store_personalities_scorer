# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 09:27:22 2018

@author: ttaylor
"""



#connect to Netezza
import sys
import pandas as pd         #import panda
sys.path.append(r'\\stphome\home\ttaylor\python')
from connect import UID,PWD    #import your netezza ID/PWD. This must be in the same location as your code.
connect_string="DRIVER={NetezzaSQL}"+";SERVER=prodnzo.catmktg.com; PORT=5480; DATABASE=pn1ussa1;UID={uid};PWD={pwd}".format(uid=UID, pwd=PWD)
#import connection package and establish connection
import pyodbc                  #import package
conn = pyodbc.connect(connect_string)  #establish a connection
cursor = conn.cursor()         #open a cursor                                   


                            
def load_and_score_storepersonality(strStorePersonalityGroup, strStorePersonality):
    ###strStorePersonalityGroup = "occupation"  ###testing values
    ###strStorePersonality = "retired"   ###testing values
    
    ##creating a temp table and then truncating it
    cursor.execute("""
                       DROP TABLE tt_store_attribute_detail if exists;
                       CREATE TEMP  TABLE  tt_store_attribute_detail as                       
                       SELECT 0::bigint as td_acct_nbr, 0::bigint as site_key,  ''::varchar(100) as store_personality_group, 
                               ''::varchar(100) as store_personality, ''::varchar(100) as attribute,
                               0::bigint as households,  0::bigint as total_households
                       DISTRIBUTE on (td_acct_nbr);   
                      TRUNCATE TABLE tt_store_attribute_detail;
                      """) 
    conn.commit()

    ##next cycle through the data we need to lookup.   Use the function paramaters to change the query
    strsql =  """
                SELECT  * 
                FROM pn1ussa1..tt_store_personality_experian_xref 
                WHERE store_personality_group = '^strStorePersonalityGroup^'
                AND store_personality = '^strStorePersonality^'
                ORDER BY experian_attribute
    """
    strsql = strsql.replace("^strStorePersonalityGroup^",strStorePersonalityGroup)
    strsql = strsql.replace("^strStorePersonality^",strStorePersonality)
    dfStoreAttributes = pd.read_sql(strsql, conn)   #execute query                                
    dfStoreAttributes.head()

    ##loop through each p# and pull the attribute we're looking for.
    for index, row in dfStoreAttributes.iterrows():
        ##create a temp table where we have out attributes
        ## first half of the query is to get a count of how many people we have for the specific p# attribute we're looknig at
        ##the second half is how many people for that p# for the specific value
        strsql = """
                   insert into tt_store_attribute_detail 
                    select a.td_acct_nbr, site_key,  store_personality_group, store_personality, '^experianattribute^' as attribute, households, total_households
                    from 
                        	(select b.td_acct_nbr, site_key,   count(distinct chhid)::float  as total_households
                        	from tt_store_personality_stores   a
                        	join  tt_store_personality_storezipcodes b
                        		on a.td_acct_nbr = b.td_acct_nbr
                        	join zn1wwss1..Experian_demographic_v c
                        		on b.zipcode = c.zip_code
                        		and b.zip4 = c.zip_4
                             WHERE c.^experianattribute^  IS NOT NULL
                        	group by b.td_acct_nbr, site_key)  as a
                    join 
                        	(select b.td_acct_nbr, '^strStorePersonalityGroup^' as  store_personality_group,  '^strStorePersonality^' as store_personality , count(distinct chhid)::float  as households
                        	from tt_store_personality_storezipcodes b
                        	join zn1wwss1..Experian_demographic_v c
                        		on b.zipcode = c.zip_code
                        		and b.zip4 = c.zip_4
                            WHERE c.^experianattribute^ = '^experianvalue^'
                     GROUP by b.td_acct_nbr, store_personality_group, store_personality)   as c
                 			ON a.td_acct_nbr = c.td_acct_nbr;         
            """

        strsql = strsql.replace("^experianattribute^",row['experian_attribute'])
        strsql = strsql.replace("^experianvalue^",row['experian_value'])
        strsql = strsql.replace("^strStorePersonalityGroup^",strStorePersonalityGroup)
        strsql = strsql.replace("^strStorePersonality^",strStorePersonality)
        print(strsql)
        #load in the  percents for each households.... this should be a function where the shopper_personality_group is passed in 
        cursor.execute(strsql)   #execute query
        conn.commit()
        
    ##then summarize....
    strsql = """
          drop table tt_store_attribute_summary2 if exists;
          create TEMP table tt_store_attribute_summary2 as
          select td_acct_nbr, site_key,  store_personality_group, store_personality,   
                    sum(households) as households, sum(total_households) as total_households, sum(households)::float /  sum(total_households)::float as percent_households
                    from tt_store_attribute_detail
                    GROUP BY td_acct_nbr, site_key,  store_personality_group, store_personality
    """
    cursor.execute(strsql)   #execute query       
    conn.commit()
        
    ### Johns approach to spread out results
    dfAttributes = pd.read_sql("""                            
                                select td_acct_nbr, site_key, store_personality_group,  store_personality, log(percent_households) - (1.0 - log(percent_households))  as percent_households
                                from tt_store_attribute_summary2
                                """, conn)   #execute query                                
    dfAttributes.head()

    ###now lets calculate zscores for attribute
    ###dfAttributes.hist(column="percent_households")
    decMean = dfAttributes["percent_households"].mean()
    decSTD = dfAttributes["percent_households"].std()
    ###print(decMean)
    ###print(decSTD)


    ###remove any previous values of what we're about to enter
    strsql = """
        DROP TABLE tt_attribute_scoring IF EXISTS;
        CREATE TEMP TABLE tt_attribute_scoring AS
        select td_acct_nbr, site_key, store_personality_group,  store_personality, log(percent_households) - (1.0 - log(percent_households))  as percent_households,
            0.0::double as zscore, ''::varchar(100) as strength, 
            current_date as date_calculated
        from tt_store_attribute_summary2
    """
    cursor.execute(strsql)   #execute query       
    conn.commit()
        

    ###dfAttributes['zscore'] = ((dfAttributes["percent_households"] - decMean) / decSTD)
    ###dfAttributes.describe()

    ###Create zscore
    strsql = "UPDATE tt_attribute_scoring  "
    strsql = strsql + "SET zscore = ((percent_households - "  + str(decMean) + ") / " + str(decSTD) + ") "
    cursor.execute(strsql)   #execute query       
    conn.commit()

    #create strengths
    strsql = "UPDATE tt_attribute_scoring  "
    strsql = strsql + "SET strength = 'strong'  "
    strsql = strsql + "WHERE zscore > 1.5  "
    cursor.execute(strsql)   #execute query       
    conn.commit()
    
    strsql = "UPDATE tt_attribute_scoring  "
    strsql = strsql + "SET strength = 'weak' "
    strsql = strsql + "WHERE zscore < -1.5  "
    cursor.execute(strsql)   #execute query       
    conn.commit()    

    strsql = "UPDATE tt_attribute_scoring  "
    strsql = strsql + "SET strength = 'moderate'  "
    strsql = strsql + "WHERE zscore between -1.5 and 1.5  "
    cursor.execute(strsql)   #execute query       
    conn.commit()

    ###save results   
    strsql = "INSERT INTO tt_store_personality_master  "
    strsql = strsql + "SELECT * FROM tt_attribute_scoring  "
    cursor.execute(strsql)   #execute query       
    conn.commit()
    
####end of function

####-------------------------------------------------------------
####  main function
###
###
strsql = "select distinct a.store_personality_group, a.store_personality "
strsql = strsql + "from pn1ussa1..tt_store_personality_experian_xref a  "
strsql = strsql + "LEFT JOIN tt_store_personality_master b "
strsql = strsql + "     ON a.store_personality_group = b.store_personality_group "
strsql = strsql + "     AND a.store_personality = b.store_personality "
strsql = strsql + "WHERE b.store_personality_group IS NULL "
strsql = strsql + "AND b.store_personality IS NULL "
dfStorePersonalities = pd.read_sql(strsql, conn)   #execute query                                
dfStorePersonalities.head()
for index, row in dfStorePersonalities.iterrows():
    spgroup = row['store_personality_group']
    sp = row['store_personality']
    print("processing " + spgroup + ":" + sp)
    load_and_score_storepersonality(spgroup,sp)

